import './App.scss';
import { TaskProvider } from './components/TaskProvider';
import { TaskListComponent } from './components/TaskListComponent';
import { AddTaskComponent } from './components/AddTaskComponent';
import { useState } from 'react';
import { ShowValueComponent } from './components/ShowValueComponent';
import { UseMemoComponent } from './components/UseMemoComponent';

function App() {
  const [state, setState] = useState('');

  const handleChange = (e) => setState(e.target.value);

  console.log('rerender', state);
  return (
    <div className="App">
      <TaskProvider>
        <h1>My Todo List</h1>
        <div className="container">
          <AddTaskComponent />
          <TaskListComponent />
        </div>
      </TaskProvider>
      <hr />
      <div className="container">
        <input
          placeholder="add changing value"
          onChange={handleChange}
          value={state}
        />
        <ShowValueComponent />
        <hr />
        <UseMemoComponent />
      </div>
    </div>
  );
}

export default App;
