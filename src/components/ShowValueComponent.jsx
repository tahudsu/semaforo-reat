import React, {memo, useState} from 'react';


function areEqual(prevProps, nextProps) {
  /*
  retorna true si al pasar los nextProps a renderizar retorna
  el mismo resultado que al pasar los prevProps a renderizar,
  de otro modo retorna false
  */
}
// memo update component when props changes, if inner state update, the component rerender.

// second param in memo, comparisionMethod, it does to check comparision
export const ShowValueComponent = memo(() => {
  const [inputValue, setInputValue] = useState("")
  const handleChange = (e) => {
    setInputValue(e.target.value);
  }

  console.log("rerender ShowValueComponent", inputValue);
  
  return (
    <div>
      <input placeholder="inner value" value={inputValue} onChange={handleChange}/>
      <span>show value: {inputValue}</span>
    </div>
  )
}, areEqual);
