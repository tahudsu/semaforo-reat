import React, { createContext, useContext, useState } from 'react'
import { v4 } from 'uuid';

// build context from react, name of context TaskContext
const TaskContext = createContext();

// init context with its provider, recieve provider TaskContext
export const useTasks = () => useContext(TaskContext);

export const TaskProvider = ({children}) => {

  const [tasks, setTasks] = useState([]);

  const addTask = (task) => {
    setTasks([...tasks, {
      id: v4(),
      task,
      complete: false
    }]);
  }

  const setStatusTask = (id, status) => {
    setTasks(tasks.map(task => task.id === id ? {...task,complete: status} : task))
  }

  const deleteTask = (id) => {
    setTasks(tasks.filter(task => task.id !== id));
  }

  // wrap content, render childs behind
  // value, passing all vars that are inside of context, included methods to be 
  return (
    <TaskContext.Provider  value={{ tasks, addTask, setStatusTask, deleteTask }}>
      {children}
    </TaskContext.Provider>
  )
}
