import React, { useState } from 'react'
import { useTasks } from './TaskProvider';

export const AddTaskComponent = () => {
  const [value, setValue] = useState('')
  const { addTask } = useTasks();
  const handleChange = (e) => {
    setValue(e.target.value);
  }

  const addNewTask = () => {
    addTask(value);
    setValue('');
  }

  const handleKeyDown = (event) => {
    console.log(event)
    if (event.key === 'Enter') {
      addNewTask()
    }
  }
  return (
    <div>
      <input type="text" placeholder="add new task" onKeyDown={handleKeyDown}  onChange={handleChange} value={value }/>
      <button onClick={addNewTask}>Add</button>
    </div>
  )
}
