import axios from 'axios'
import React, { useEffect, useMemo, useState } from 'react'

export const UseMemoComponent = () => {

  const [dateSearch, setDateSearch] = useState();
  useMemo((dateSearch) => getData(), dateSearch)
  useEffect(() => {
    getData();
  }, [])
  // const memoizedResult = useMemo(compute, dependencies);
  const getData = async () => {
    // https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY
    const newDate = new Date();
    const dateFormated = newDate.toISOString().split('T')[0];
    // YYYY-MM-DD
    const res = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=${dateFormated}`);
    console.log(res.data);
  }
  
  return (
    <div>
      UseMemoComponent
      <hr />
    </div>
  )
}
