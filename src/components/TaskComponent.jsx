import React from 'react'
import { useTasks } from './TaskProvider'

export const TaskComponent = ({id, task, complete}) => {
  const { setStatusTask, deleteTask } = useTasks();

  const checkTask = (event) => setStatusTask(id, event.target.checked);

  const removeTask = (event) => deleteTask(id);

  return (
    <tr>
      <td>
        <input type="checkbox" onChange={checkTask} />
      </td>
      <td>
        <span className={ complete ? 'task-done' : ''}>{task}</span>
      </td>
      <td>
        <button onClick={removeTask}>X</button>
      </td>
    </tr>
  )
}
