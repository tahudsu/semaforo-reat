import React from 'react'
import { TaskComponent } from './TaskComponent';
import { useTasks } from './TaskProvider'

export const TaskListComponent = () => {
  const { tasks } = useTasks();
  console.log(tasks);
  return (
    <table>
      <tbody>
        {tasks.map((task, index) => <TaskComponent key={index} {...task} />)}
      </tbody>
    </table>
  )
}
